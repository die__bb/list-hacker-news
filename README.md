# List Hacker News

Demo app using React in frontend, NodeJS + Express in backend, MongoDB and Jest for unit testing
running in Dcoker containers.


## Prerequisites

- **Docker**: You can download and install following the steps [here](https://docs.docker.com/compose/gettingstarted/).
- **Dcker Compose**: You can download and install following the steps [here](https://docs.docker.com/compose/gettingstarted/).
- This project use the ports 3000 and 4000 for serve and running the application, so make sure
no other app is running in those ports. 
## Installation

1. Clone the repo:
    ```
    $ git clone https://gitlab.com/die__bb/list-hacker-news.git
    ```
2. Change the working directory:
    ```
    $ cd list-hacker-news/
    ```
3. Build the images with Docker Compose:
    ```
    $ docker-compose build
    ``` 
    If the last one fails, try with sudo:
    ```
    $ sudo docker-compose build
    ```
 4. Run the docker containers:
    ```
     $ docker-compose up
    ```
5. If everything went well, open a new tab and navigate to [http://localhost:4000](http://localhost:4000) and you 
should see the web application.

