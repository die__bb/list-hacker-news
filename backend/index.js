const mongoose = require('mongoose');
const app = require('./server');
const bodyParser= require('body-parser');
const environment = require('./environments');
const express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const postRouter = require('./posts/post.router');
const userPostRouter = require('./userPosts/userPost.router');
const axios = require('axios');
const cron = require("node-cron");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
mongoose.connect(environment.DATABASE);
mongoose.Promise = global.Promise;

mongoose.connection.on('connected', () => {
	console.info('Mongoose connected!')
});

mongoose.connection.on('disconnected', () => {
	console.info('Mongoose disconnected!');
});

mongoose.connection.on('error', (err) => {
	console.error(err.message);
	process.exit(1);
});

require('./posts/post.model');
require('./userPosts/userPost.model');
app.use(session({
	secret: 'reign-secret',
	saveUninitialized: true,
	resave: true,
	store: new MongoStore({ mongooseConnection: mongoose.connection })
}));
function getData  () { //Fetch posts from Hacker News
	axios.get(environment.HN_URL)
		.then(response => {
				console.info(response.data.hits.length);
				postRouter.insert(response.data.hits).then(r => {})
			}
		)
		.catch(err => console.error(err));
}
getData(); // Populate the DB for the first time
cron.schedule('0 * * * *', () => { // Schedule fetching data every hour
	getData()
});


app.use('/api/posts', postRouter.router);
app.use('/api/delete', userPostRouter.router);
app.listen(environment.PORT, () => {
	Object.keys(environment).forEach((key) => console.info(`${key}: ${environment[key]}`));
});
