const express = require('express');
const catchErrors = require('express-catch-errors');

const router = express.Router();

const { insert, list } = require('./post.controller');

router.route('/')
	.get(catchErrors(list));

module.exports = { router, insert };
