
const mongoose = require('mongoose');

const PostsSchema = new mongoose.Schema({
	title: { type: String, required: true},
	story_id: {type: Number, required: true},
	url: String,
	date: Date,
	author: String
});

const Post = mongoose.model('Post', PostsSchema);

module.exports = Post;
