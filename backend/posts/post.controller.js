const Post = require('./post.model');
const UserPostRouter = require('../userPosts/userPost.router');

module.exports.insert = async (posts) => {
	console.debug('Inserting posts ', posts.length);
	const postsFiltered = posts.filter((item, index) => posts.findIndex(e =>e.story_id === item.story_id) === index);
	const promises = postsFiltered.map(async post => {
		const title = post.story_title || post.title;
		if (title) {
			await Post.updateOne({ "story_id": post.story_id || post.objectID }, {
					'$set': {},
					"$setOnInsert": {
						"story_id": post.story_id || post.objectID,
						"title": title,
						"url": post.story_url || post.url,
						"author": post.author,
						"date": post.created_at
					}
				},
			{ upsert: true }
			)
		}
	});
	return Promise.all(promises);
};

module.exports.list = async (req, res) => { // List posts filtered
	const userPostsDeleted = req.sessionID ? await UserPostRouter.get(req.sessionID) : [];
	console.debug('User posts deleted ', userPostsDeleted);
	const posts = await Post.find({ story_id: { $nin: userPostsDeleted} }).sort({date: -1});
	res.json(posts);
};
