const mongoose = require('mongoose');
const PostModel = require('./post.model');
const PostController = require('./post.controller');
const postData = { "story_id": 1123245,
	"title": "title test",
	"url": "https://www.reign.cl/es/",
	"author": "diego test",
	"date": new Date("2020-08-15T10:35:00.000Z") };
const newPosts = [{
	"story_id": 1123246,
	"title": "title test2",
	"url": "https://www.reign.cl/es/",
	"author": "diego test2",
	"date": new Date("2020-08-15T10:35:00.000Z")
}, {
	"story_id": 1123247,
	"title": "title test3",
	"url": "https://www.reign.cl/es/",
	"author": "diego test3",
	"date": new Date("2020-08-15T10:35:00.000Z")
}];
const environment = require('../environments');

describe('Post Model Test', () => {
	// mongoose connect
	beforeAll(async () => {
		await mongoose.connect(environment.DATABASE, { useNewUrlParser: true, useCreateIndex: true }, (err) => {
			if (err) {
				console.error(err);
				process.exit(1);
			}
		});
	});
	it('create & save Post successfully', async () => {
		const validPost = new PostModel(postData);
		const savedPost = await validPost.save();
		// Object Id should be defined when successfully saved to MongoDB.
		expect(savedPost._id).toBeDefined();
		expect(savedPost.story_id).toBe(postData.story_id);
		expect(savedPost.title).toBe(postData.title);
		expect(savedPost.url).toBe(postData.url);
		expect(savedPost.author).toBe(postData.author);
		expect(savedPost.date).toBe(postData.date);
	});
	it('insert Post successfully, but the field does not defined in schema should be undefined', async () => {
		const postWithInvalidField = new PostModel({
			title: 'titulo test 2', url: 'url.test', author: 'test author', date: new Date("2020-08-15T10:35:00.000Z"),
			badColumn: 'field not defined', story_id: 1111111 });
		const savedPostWithInvalidField = await postWithInvalidField.save();
		expect(savedPostWithInvalidField._id).toBeDefined();
		expect(savedPostWithInvalidField.badColumn).toBeUndefined();
	});
	it('create Post without required field should failed', async () => {
		const postWithoutRequiredField = new PostModel({ title: 'title test', url: 'url.test', author: 'test author' });
		let err;
		try {
			error = await postWithoutRequiredField.save();
		} catch (error) {
			err = error
		}
		expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
		expect(err.errors.story_id).toBeDefined();
	});
	
	// Test controllers
	it ('simulate inserting posts from HN API', async () => {
		const postsInserted = await PostController.insert(newPosts.concat(postData));
		expect(postsInserted.length).toBe(3)
	});
	afterAll(done => {
		// Closing the DB connection allows Jest to exit successfully.
		mongoose.connection.close();
		done()
	})
});
