require('dotenv').config();

const environment = ['NODE_ENV', 'DATABASE', 'PORT', 'HN_URL', 'FRONT_URL'];

environment.forEach((name) => {
	if (!process.env[name]) {
		throw new Error(`${name}: ${process.env[name]}`);
	}
});

module.exports = {
	NODE_ENV: process.env.NODE_ENV,
	DATABASE: process.env.DATABASE,
	PORT: process.env.PORT,
	HN_URL: process.env.HN_URL,
	FRONT_URL: process.env.FRONT_URL
};
