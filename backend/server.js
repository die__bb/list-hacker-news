const express = require('express');
const app = express();
const environment = require('./environments');
const cors = require("cors");
const whitelist = [environment.FRONT_URL];//'http://localhost:4000'];
const corsOpt = {
	credentials: true,
	origin: ((origin, callback) => {
		console.log(origin);
		if (whitelist.indexOf(origin) !== -1) {
			callback(null, true)
		} else {
			callback(new Error('Not allowed by CORS'))
		}
	})
};

app.use(cors(corsOpt));

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Credentials", true);
	res.header("Access-Control-Allow-Origin",environment.FRONT_URL );//req.headers.origin);
	res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
	res.header(
		"Access-Control-Allow-Headers", 'Authorization, X-API-KEY, Origin,X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Access-Control-Allow-Request-Method '
	);
	if ("OPTIONS" === req.method) {
		res.send(200);
	} else {
		next();
	}
});

//
module.exports = app;
