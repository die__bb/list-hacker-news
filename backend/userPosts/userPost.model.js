const mongoose = require('mongoose');

const UserPostSchema = new mongoose.Schema({
	userSessionId: { type: String, required: true },
	postDeletedId: { type: Number, required: true }
});

const UserPost = mongoose.model('UserPost', UserPostSchema);

module.exports = UserPost;
