const mongoose = require('mongoose');
const UserPostModel = require('./userPost.model');
const userPostData = {
	"userSessionId": 'abcdfg1234',
	"postDeletedId": 1123245
};
const environment = require('../environments');

describe('User Posts Model Test', () => {
	beforeAll(async () => {
		await mongoose.connect(environment.DATABASE, { useNewUrlParser: true, useCreateIndex: true }, (err) => {
			if (err) {
				console.error(err);
				process.exit(1);
			}
		});
	});
	it('create & save UserPost successfully', async () => {
		const validUserPost = new UserPostModel(userPostData);
		const savedUserPost = await validUserPost.save();
		// Object Id should be defined when successfully saved to MongoDB.
		expect(savedUserPost._id).toBeDefined();
		expect(savedUserPost.userSessionId).toBe(userPostData.userSessionId);
		expect(savedUserPost.postDeletedId).toBe(userPostData.postDeletedId);

	});
	it('insert UserPost successfully, but the field does not defined in schema should be undefined', async () => {
		const userPostWithInvalidField = new UserPostModel({
			userSessionId: 'asasfa123', postDeletedId: 111212, badColumn: 'field not defined' });
		const savedUserPostWithInvalidField = await userPostWithInvalidField.save();
		expect(savedUserPostWithInvalidField._id).toBeDefined();
		expect(savedUserPostWithInvalidField.badColumn).toBeUndefined();
	});
	it('create UserPost without required field should failed', async () => {
		const postWithoutRequiredField = new UserPostModel({ userSessionId: 'asasfa123' });
		let err;
		try {
			error = await postWithoutRequiredField.save();
		} catch (error) {
			err = error
		}
		expect(err).toBeInstanceOf(mongoose.Error.ValidationError);
		expect(err.errors.postDeletedId).toBeDefined();
	});
	afterAll(done => {
		// Closing the DB connection allows Jest to exit successfully.
		mongoose.connection.close();
		done()
	})
});
