const UserPost = require('./userPost.model');

module.exports.insert = async (req, res) => { // "Inserting" the deleted posts
	console.debug('Inserting post id to filter ', req.body.postId);
	await UserPost.updateOne({ "userSessionId": req.sessionID, "postDeletedId": req.body.postId }, {
			'$set': {},
			"$setOnInsert": {
				"userSessionId": req.sessionID,
				"postDeletedId": req.body.postId
			}
		},{
			upsert: true
		}
		);
	res.json({res: 'OK'})
};

module.exports.get = async (sessionId) => { // Get the post deleted
	const postsDeleted = await UserPost.find({ "userSessionId": sessionId }, { postDeletedId: 1, _id:0 });
	return postsDeleted.map(postId => postId.postDeletedId )
//, "postDeletedId": postId }).promise()
};
