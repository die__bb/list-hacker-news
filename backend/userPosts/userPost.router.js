const express = require('express');
const catchErrors = require('express-catch-errors');

const router = express.Router();

const { insert, get } = require('./userPost.controller');

router.route('/')
	.post(catchErrors(insert));

module.exports = { router, get };
