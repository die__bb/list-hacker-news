import Cookies from 'universal-cookie';

export default class ApiService {
	 static getPost () {
		 const cookies = new Cookies();
		return new Promise((resolve, reject) => {
			fetch(process.env.REACT_APP_BACKEND_URI + 'api/posts', {
				credentials: "include",
				headers: {
					Cookie: cookies.get('connect.sid')
				}
			})
				.then(response => {
					resolve(response.json())
				}).catch(e => {
					console.error(e);
					reject(e)
			})
		})
	}
	static deletePost (postId) {
		let body = { 'postId': postId };
		return new Promise((resolve, reject) => {
			fetch(process.env.REACT_APP_BACKEND_URI + 'api/delete', {
				credentials: "include",
				method: 'POST',
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(body)
			})
				.then(response => {
					resolve(response.json())
				}).catch(e => {
				console.error(e);
				reject(e)
			})
		})
	}
}
