import React, { Component } from "react";
import Moment from 'moment';
import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
class Post  extends Component{
	today = Moment();
	yesterday = this.today.clone().subtract(1, 'days');
	constructor(props) {
		super(props);
		this.state = {
			date: ''
		};
		this.handleDeletePost = this.handleDeletePost.bind(this)
	}
	componentDidMount () {
		this.formatDate(this.props.date)
	}
	isToday = (momentDate) => {
		return momentDate.isSame(this.today, 'd')
	};
	isYesterday = (momentDate) => {
		return momentDate.isSame(this.yesterday, 'd')
	};
	formatDate = (date) => {
		let newDate = this.isToday(Moment(date)) ? Moment(date).format('HH:MM A') :
	   this.isYesterday(Moment(date)) ? 'Yesterday' :  Moment(date).format('MMM D');
		this.setState({ date: newDate })
	};
	handleDeletePost(e) {
		e.preventDefault();
		this.props.handleDelete(this.props.story_id)
	};
	render () {
		return (
			<div className={"box-post"}>
				<a href={this.props.url} target={"_blank"} rel={"noopener noreferrer"} className={"links-post"}
				   disabled={typeof this.props.url === "undefined"}       /* fullWidth={true} */>
				<Box alignItems={'center'} display="flex"  >
					<Box flexGrow={1} className={"box-title"}>
						{this.props.title}
					</Box>
					<Box flexGrow={1} className={"box-author"}>
						- {this.props.author} -
					</Box>
					<Box className={"box-date"} >
						{this.state.date}
					</Box>
					<Box className={'box-icon'}>
						<IconButton  onClick={this.handleDeletePost} >
							<DeleteIcon className={"delete-icon"}/>
						</IconButton>
						
					</Box>
				</Box>
				
				</a>
			</div>
		);
	}
}
export default Post;
