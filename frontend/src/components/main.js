import React, { Component } from "react";
import ApiService from '../services/apiService';
import Post from './posts';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';


class Main  extends Component{
	constructor(props) {
		super(props);
		this.state = {
			loaded: false,
			posts: []
		};
		this.handleDelete = this.handleDelete.bind(this)
	}
	componentDidMount () {
		ApiService.getPost()
			.then(res => {
				console.log('res ', res);
				this.setState({ posts: res, loaded: true })
			})
		
	}
	handleDelete(postId) {
		console.log('borrando postid ', postId);
		ApiService.deletePost(postId)
			.then(res => {
				const newPosts = this.state.posts.filter(post => post.story_id !== postId);
				this.setState({ posts: newPosts });
				console.log('Post Id deleted')
			})
	}
	render () {
		return (this.state.loaded ?
			<div>
				<header>
					<Container disableGutters={true} maxWidth={false}>
						<Typography component="div" style={{ backgroundColor: '#333333' }} >
							<div className="header-title">
								<h2>HN Feed</h2>
								<h4>We &#60;3 hacker news!</h4>
							</div>
						</Typography>
					</Container>
				</header>
				
				<ul>
					{this.state.posts.map(post =>
						<li key={post.story_id}>
							<Post title={post.title} author={post.author} story_id={post.story_id}
							 handleDelete={this.handleDelete}   date={post.date} url={post.url}/>
						</li>
					)}
				</ul>
			</div>
				:
				<h2>cargando...</h2>
		)
	}
}
export default Main;
